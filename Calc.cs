using System;

namespace basic_calc
{
    class calc
    {
        /*
        public static void Main(string[] args)
        {
            if (args.Length == 0 ) {
                Console.WriteLine(" Hello user. \n You can calculate 2 arguments. \n Available operations: (+, -, *, /).");
                Console.Write("-> ");

                args[0] = Console.ReadLine();
            } 

            Console.WriteLine(args[0]);
            //Console.WriteLine(OutString(args));
        }
        */

        public static void Main(string[] args)
        {
            Console.WriteLine(" Hello user. \n You can calculate 2 arguments. \n Available operations: (+, -, *, /).");
            Console.Write("-> ");             

            Console.WriteLine(OutString(Console.ReadLine()));
        }

        private static string OutString(string str)
        {
            string[] numbers = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            float a, b, result;
            
            try 
            {
                a = float.Parse(numbers[0]);
                b = float.Parse(numbers[2]);
            } catch (ArgumentException){
                Console.WriteLine("Wrong numbers.");
                return "Program stopped.";
            }

            //HashSet<char> evenNumbers = new HashSet<char>('+','-','*','/');
            

            switch (numbers[1])
            {
                case "+":
                    result = a + b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "/":
                    result = a / b;
                    break;
                default:
                    return "Error";
            }
                    
            return ("-> " + Convert.ToString(result));
        }
    }
}